-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2020 at 04:24 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `college`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-10-06 11:26:41', '2020-10-06 11:26:41', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://college.test', 'yes'),
(2, 'home', 'http://college.test', 'yes'),
(3, 'blogname', 'college', 'yes'),
(4, 'blogdescription', 'Best University web site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'hugoman@abv.bg', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '3', 'yes'),
(23, 'date_format', 'd/m/Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:143:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:9:\"events/?$\";s:25:\"index.php?post_type=event\";s:39:\"events/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=event&feed=$matches[1]\";s:34:\"events/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=event&feed=$matches[1]\";s:26:\"events/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=event&paged=$matches[1]\";s:11:\"programs/?$\";s:27:\"index.php?post_type=program\";s:41:\"programs/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=program&feed=$matches[1]\";s:36:\"programs/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=program&feed=$matches[1]\";s:28:\"programs/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=program&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:34:\"events/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"events/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"events/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"events/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"events/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"events/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"events/([^/]+)/embed/?$\";s:38:\"index.php?event=$matches[1]&embed=true\";s:27:\"events/([^/]+)/trackback/?$\";s:32:\"index.php?event=$matches[1]&tb=1\";s:47:\"events/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?event=$matches[1]&feed=$matches[2]\";s:42:\"events/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?event=$matches[1]&feed=$matches[2]\";s:35:\"events/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?event=$matches[1]&paged=$matches[2]\";s:42:\"events/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?event=$matches[1]&cpage=$matches[2]\";s:31:\"events/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?event=$matches[1]&page=$matches[2]\";s:23:\"events/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"events/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"events/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"events/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"events/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"events/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"programs/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"programs/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"programs/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"programs/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"programs/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"programs/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"programs/([^/]+)/embed/?$\";s:40:\"index.php?program=$matches[1]&embed=true\";s:29:\"programs/([^/]+)/trackback/?$\";s:34:\"index.php?program=$matches[1]&tb=1\";s:49:\"programs/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?program=$matches[1]&feed=$matches[2]\";s:44:\"programs/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?program=$matches[1]&feed=$matches[2]\";s:37:\"programs/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?program=$matches[1]&paged=$matches[2]\";s:44:\"programs/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?program=$matches[1]&cpage=$matches[2]\";s:33:\"programs/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?program=$matches[1]&page=$matches[2]\";s:25:\"programs/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"programs/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"programs/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"programs/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"programs/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"programs/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=45&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:30:\"advanced-custom-fields/acf.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '3', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'university', 'yes'),
(41, 'stylesheet', 'university', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '48748', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '0', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:0:{}', 'yes'),
(78, 'widget_rss', 'a:0:{}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '47', 'yes'),
(82, 'page_on_front', '45', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1617535601', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'initial_db_version', '48748', 'yes'),
(96, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(97, 'fresh_site', '0', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:7:{i:1605108402;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1605137202;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1605180402;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1605180417;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1605180418;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1605698802;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(119, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1601986451;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(130, 'can_compress_scripts', '1', 'no'),
(143, 'finished_updating_comment_type', '1', 'yes'),
(146, 'current_theme', 'University Theme', 'yes'),
(147, 'theme_mods_university', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:3:{s:20:\"header-menu-location\";i:2;s:22:\"footer-menu-location-1\";i:3;s:22:\"footer-menu-location-2\";i:4;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(148, 'theme_switched', '', 'yes'),
(152, 'WPLANG', '', 'yes'),
(153, 'new_admin_email', 'hugoman@abv.bg', 'yes'),
(171, '_transient_health-check-site-status-result', '{\"good\":12,\"recommended\":8,\"critical\":0}', 'yes'),
(190, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(227, 'category_children', 'a:0:{}', 'yes'),
(280, 'recently_activated', 'a:0:{}', 'yes'),
(281, 'acf_version', '5.9.3', 'yes'),
(300, 'recovery_mode_email_last_sent', '1602767566', 'yes'),
(352, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.3-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.5.3\";s:7:\"version\";s:5:\"5.5.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1605108161;s:15:\"version_checked\";s:5:\"5.5.3\";s:12:\"translations\";a:0:{}}', 'no'),
(355, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:14:\"hugoman@abv.bg\";s:7:\"version\";s:5:\"5.5.3\";s:9:\"timestamp\";i:1604587444;}', 'no'),
(356, '_site_transient_timeout_php_check_90a84b80b2ae4bb2a14bb18a1c790012', '1605192247', 'no'),
(357, '_site_transient_php_check_90a84b80b2ae4bb2a14bb18a1c790012', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(371, '_site_transient_timeout_browser_46abf0d68e56d79f1380ffaab81adce5', '1605531638', 'no'),
(372, '_site_transient_browser_46abf0d68e56d79f1380ffaab81adce5', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"86.0.4240.183\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(403, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1605108163;s:7:\"checked\";a:1:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.9.3\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:1:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.9.3\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(429, '_site_transient_timeout_theme_roots', '1605109962', 'no'),
(430, '_site_transient_theme_roots', 'a:1:{s:10:\"university\";s:7:\"/themes\";}', 'no'),
(431, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1605108163;s:7:\"checked\";a:1:{s:10:\"university\";s:0:\"\";}s:8:\"response\";a:1:{s:10:\"university\";a:6:{s:5:\"theme\";s:10:\"university\";s:11:\"new_version\";s:3:\"1.3\";s:3:\"url\";s:40:\"https://wordpress.org/themes/university/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/theme/university.1.3.zip\";s:8:\"requires\";b:0;s:12:\"requires_php\";b:0;}}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_lock', '1601989081:1'),
(5, 7, '_edit_lock', '1602683753:1'),
(7, 9, '_edit_lock', '1602073734:1'),
(9, 13, '_edit_lock', '1602074076:1'),
(10, 15, '_edit_lock', '1604930026:1'),
(11, 3, '_edit_lock', '1602075878:1'),
(12, 16, '_edit_lock', '1602075915:1'),
(13, 19, '_edit_lock', '1602081908:1'),
(14, 21, '_edit_lock', '1602077838:1'),
(15, 23, '_menu_item_type', 'custom'),
(16, 23, '_menu_item_menu_item_parent', '0'),
(17, 23, '_menu_item_object_id', '23'),
(18, 23, '_menu_item_object', 'custom'),
(19, 23, '_menu_item_target', ''),
(20, 23, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(21, 23, '_menu_item_xfn', ''),
(22, 23, '_menu_item_url', 'http://college.test/'),
(23, 23, '_menu_item_orphaned', '1602085091'),
(24, 24, '_menu_item_type', 'post_type'),
(25, 24, '_menu_item_menu_item_parent', '0'),
(26, 24, '_menu_item_object_id', '16'),
(27, 24, '_menu_item_object', 'page'),
(28, 24, '_menu_item_target', ''),
(29, 24, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(30, 24, '_menu_item_xfn', ''),
(31, 24, '_menu_item_url', ''),
(32, 24, '_menu_item_orphaned', '1602085091'),
(33, 25, '_menu_item_type', 'post_type'),
(34, 25, '_menu_item_menu_item_parent', '0'),
(35, 25, '_menu_item_object_id', '15'),
(36, 25, '_menu_item_object', 'page'),
(37, 25, '_menu_item_target', ''),
(38, 25, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(39, 25, '_menu_item_xfn', ''),
(40, 25, '_menu_item_url', ''),
(41, 25, '_menu_item_orphaned', '1602085091'),
(42, 26, '_menu_item_type', 'post_type'),
(43, 26, '_menu_item_menu_item_parent', '0'),
(44, 26, '_menu_item_object_id', '19'),
(45, 26, '_menu_item_object', 'page'),
(46, 26, '_menu_item_target', ''),
(47, 26, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(48, 26, '_menu_item_xfn', ''),
(49, 26, '_menu_item_url', ''),
(50, 26, '_menu_item_orphaned', '1602085091'),
(51, 27, '_menu_item_type', 'post_type'),
(52, 27, '_menu_item_menu_item_parent', '0'),
(53, 27, '_menu_item_object_id', '13'),
(54, 27, '_menu_item_object', 'page'),
(55, 27, '_menu_item_target', ''),
(56, 27, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(57, 27, '_menu_item_xfn', ''),
(58, 27, '_menu_item_url', ''),
(59, 27, '_menu_item_orphaned', '1602085091'),
(60, 28, '_menu_item_type', 'post_type'),
(61, 28, '_menu_item_menu_item_parent', '0'),
(62, 28, '_menu_item_object_id', '21'),
(63, 28, '_menu_item_object', 'page'),
(64, 28, '_menu_item_target', ''),
(65, 28, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(66, 28, '_menu_item_xfn', ''),
(67, 28, '_menu_item_url', ''),
(68, 28, '_menu_item_orphaned', '1602085091'),
(69, 29, '_menu_item_type', 'post_type'),
(70, 29, '_menu_item_menu_item_parent', '0'),
(71, 29, '_menu_item_object_id', '2'),
(72, 29, '_menu_item_object', 'page'),
(73, 29, '_menu_item_target', ''),
(74, 29, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(75, 29, '_menu_item_xfn', ''),
(76, 29, '_menu_item_url', ''),
(77, 29, '_menu_item_orphaned', '1602085091'),
(78, 30, '_menu_item_type', 'post_type'),
(79, 30, '_menu_item_menu_item_parent', '0'),
(80, 30, '_menu_item_object_id', '9'),
(81, 30, '_menu_item_object', 'page'),
(82, 30, '_menu_item_target', ''),
(83, 30, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(84, 30, '_menu_item_xfn', ''),
(85, 30, '_menu_item_url', ''),
(86, 30, '_menu_item_orphaned', '1602085091'),
(87, 31, '_menu_item_type', 'custom'),
(88, 31, '_menu_item_menu_item_parent', '0'),
(89, 31, '_menu_item_object_id', '31'),
(90, 31, '_menu_item_object', 'custom'),
(91, 31, '_menu_item_target', ''),
(92, 31, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(93, 31, '_menu_item_xfn', ''),
(94, 31, '_menu_item_url', 'http://college.test/'),
(95, 31, '_menu_item_orphaned', '1602085115'),
(96, 32, '_menu_item_type', 'post_type'),
(97, 32, '_menu_item_menu_item_parent', '0'),
(98, 32, '_menu_item_object_id', '16'),
(99, 32, '_menu_item_object', 'page'),
(100, 32, '_menu_item_target', ''),
(101, 32, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(102, 32, '_menu_item_xfn', ''),
(103, 32, '_menu_item_url', ''),
(104, 32, '_menu_item_orphaned', '1602085115'),
(105, 33, '_menu_item_type', 'post_type'),
(106, 33, '_menu_item_menu_item_parent', '0'),
(107, 33, '_menu_item_object_id', '15'),
(108, 33, '_menu_item_object', 'page'),
(109, 33, '_menu_item_target', ''),
(110, 33, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(111, 33, '_menu_item_xfn', ''),
(112, 33, '_menu_item_url', ''),
(113, 33, '_menu_item_orphaned', '1602085115'),
(114, 34, '_menu_item_type', 'post_type'),
(115, 34, '_menu_item_menu_item_parent', '0'),
(116, 34, '_menu_item_object_id', '19'),
(117, 34, '_menu_item_object', 'page'),
(118, 34, '_menu_item_target', ''),
(119, 34, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(120, 34, '_menu_item_xfn', ''),
(121, 34, '_menu_item_url', ''),
(122, 34, '_menu_item_orphaned', '1602085115'),
(123, 35, '_menu_item_type', 'post_type'),
(124, 35, '_menu_item_menu_item_parent', '0'),
(125, 35, '_menu_item_object_id', '13'),
(126, 35, '_menu_item_object', 'page'),
(127, 35, '_menu_item_target', ''),
(128, 35, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(129, 35, '_menu_item_xfn', ''),
(130, 35, '_menu_item_url', ''),
(131, 35, '_menu_item_orphaned', '1602085115'),
(132, 36, '_menu_item_type', 'post_type'),
(133, 36, '_menu_item_menu_item_parent', '0'),
(134, 36, '_menu_item_object_id', '21'),
(135, 36, '_menu_item_object', 'page'),
(136, 36, '_menu_item_target', ''),
(137, 36, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(138, 36, '_menu_item_xfn', ''),
(139, 36, '_menu_item_url', ''),
(140, 36, '_menu_item_orphaned', '1602085115'),
(141, 37, '_menu_item_type', 'post_type'),
(142, 37, '_menu_item_menu_item_parent', '0'),
(143, 37, '_menu_item_object_id', '2'),
(144, 37, '_menu_item_object', 'page'),
(145, 37, '_menu_item_target', ''),
(146, 37, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(147, 37, '_menu_item_xfn', ''),
(148, 37, '_menu_item_url', ''),
(149, 37, '_menu_item_orphaned', '1602085115'),
(150, 38, '_menu_item_type', 'post_type'),
(151, 38, '_menu_item_menu_item_parent', '0'),
(152, 38, '_menu_item_object_id', '9'),
(153, 38, '_menu_item_object', 'page'),
(154, 38, '_menu_item_target', ''),
(155, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(156, 38, '_menu_item_xfn', ''),
(157, 38, '_menu_item_url', ''),
(158, 38, '_menu_item_orphaned', '1602085115'),
(159, 39, '_menu_item_type', 'post_type'),
(160, 39, '_menu_item_menu_item_parent', '0'),
(161, 39, '_menu_item_object_id', '16'),
(162, 39, '_menu_item_object', 'page'),
(163, 39, '_menu_item_target', ''),
(164, 39, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(165, 39, '_menu_item_xfn', ''),
(166, 39, '_menu_item_url', ''),
(168, 40, '_menu_item_type', 'post_type'),
(169, 40, '_menu_item_menu_item_parent', '0'),
(170, 40, '_menu_item_object_id', '13'),
(171, 40, '_menu_item_object', 'page'),
(172, 40, '_menu_item_target', ''),
(173, 40, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(174, 40, '_menu_item_xfn', ''),
(175, 40, '_menu_item_url', ''),
(177, 41, '_menu_item_type', 'post_type'),
(178, 41, '_menu_item_menu_item_parent', '0'),
(179, 41, '_menu_item_object_id', '21'),
(180, 41, '_menu_item_object', 'page'),
(181, 41, '_menu_item_target', ''),
(182, 41, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(183, 41, '_menu_item_xfn', ''),
(184, 41, '_menu_item_url', ''),
(186, 42, '_menu_item_type', 'post_type'),
(187, 42, '_menu_item_menu_item_parent', '0'),
(188, 42, '_menu_item_object_id', '2'),
(189, 42, '_menu_item_object', 'page'),
(190, 42, '_menu_item_target', ''),
(191, 42, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(192, 42, '_menu_item_xfn', ''),
(193, 42, '_menu_item_url', ''),
(195, 43, '_menu_item_type', 'custom'),
(196, 43, '_menu_item_menu_item_parent', '0'),
(197, 43, '_menu_item_object_id', '43'),
(198, 43, '_menu_item_object', 'custom'),
(199, 43, '_menu_item_target', ''),
(200, 43, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(201, 43, '_menu_item_xfn', ''),
(202, 43, '_menu_item_url', 'http://google.com'),
(204, 44, '_menu_item_type', 'post_type'),
(205, 44, '_menu_item_menu_item_parent', '0'),
(206, 44, '_menu_item_object_id', '16'),
(207, 44, '_menu_item_object', 'page'),
(208, 44, '_menu_item_target', ''),
(209, 44, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(210, 44, '_menu_item_xfn', ''),
(211, 44, '_menu_item_url', ''),
(213, 45, '_edit_lock', '1602159206:1'),
(214, 47, '_edit_lock', '1602159227:1'),
(215, 49, '_edit_lock', '1602683731:1'),
(219, 52, '_edit_last', '1'),
(220, 52, '_edit_lock', '1604936115:1'),
(221, 53, '_edit_last', '1'),
(222, 53, '_edit_lock', '1605108084:1'),
(223, 54, '_edit_last', '1'),
(224, 54, '_edit_lock', '1604942922:1'),
(225, 54, 'event-date', 'July 1st 2020'),
(226, 56, '_edit_last', '1'),
(227, 56, '_edit_lock', '1604940959:1'),
(228, 54, 'event_date', '20201017'),
(229, 54, '_event_date', 'field_5f87053828e55'),
(230, 53, 'event_date', '20200731'),
(231, 53, '_event_date', 'field_5f87053828e55'),
(232, 52, 'event_date', '20201015'),
(233, 52, '_event_date', 'field_5f87053828e55'),
(234, 58, '_edit_last', '1'),
(235, 58, '_edit_lock', '1604942710:1'),
(236, 58, 'event_date', '20201231'),
(237, 58, '_event_date', 'field_5f87053828e55'),
(238, 59, '_edit_lock', '1602769432:1'),
(239, 62, '_edit_last', '1'),
(240, 62, '_edit_lock', '1605020872:1'),
(241, 62, 'event_date', '20191031'),
(242, 62, '_event_date', 'field_5f87053828e55'),
(243, 63, '_edit_last', '1'),
(244, 63, '_edit_lock', '1603112333:1'),
(245, 64, '_edit_last', '1'),
(246, 64, '_edit_lock', '1603113642:1'),
(247, 65, '_edit_last', '1'),
(248, 65, '_edit_lock', '1605108090:1'),
(249, 67, '_edit_last', '1'),
(250, 67, '_edit_lock', '1604937461:1'),
(251, 53, 'related_programs', 'a:2:{i:0;s:2:\"64\";i:1;s:2:\"63\";}'),
(252, 53, '_related_programs', 'field_5f8d844500307'),
(253, 70, '_edit_lock', '1604927079:1'),
(255, 73, '_edit_last', '1'),
(256, 73, '_edit_lock', '1605002348:1'),
(257, 76, '_edit_last', '1'),
(258, 76, '_edit_lock', '1604943080:1'),
(259, 77, '_edit_last', '1'),
(260, 77, '_edit_lock', '1605025546:1'),
(261, 77, 'event_date', '20201127'),
(262, 77, '_event_date', 'field_5f87053828e55'),
(263, 77, 'related_programs', ''),
(264, 77, '_related_programs', 'field_5f8d844500307'),
(265, 77, 'event_location', ''),
(266, 77, '_event_location', 'field_5fa967b9de4e5');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-10-06 11:26:41', '2020-10-06 11:26:41', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2020-10-06 11:26:41', '2020-10-06 11:26:41', '', 0, 'http://college.test/?p=1', 0, 'post', '', 1),
(2, 1, '2020-10-06 11:26:41', '2020-10-06 11:26:41', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://college.test/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2020-10-06 11:26:41', '2020-10-06 11:26:41', '', 0, 'http://college.test/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-10-06 11:26:41', '2020-10-06 11:26:41', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://college.test.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2020-10-06 11:26:41', '2020-10-06 11:26:41', '', 0, 'http://college.test/?page_id=3', 0, 'page', '', 0),
(5, 1, '2020-10-06 15:59:54', '2020-10-06 12:59:54', '<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->', 'Test Post', '', 'publish', 'open', 'open', '', 'test-post', '', '', '2020-10-06 15:59:54', '2020-10-06 12:59:54', '', 0, 'http://college.test/?p=5', 0, 'post', '', 0),
(6, 1, '2020-10-06 15:59:54', '2020-10-06 12:59:54', '<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->', 'Test Post', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2020-10-06 15:59:54', '2020-10-06 12:59:54', '', 5, 'http://college.test/2020/10/06/5-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2020-10-06 16:01:01', '2020-10-06 13:01:01', '<!-- wp:paragraph -->\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n<!-- /wp:paragraph -->', 'Second Test Post', '', 'publish', 'open', 'open', '', 'second-test-post', '', '', '2020-10-06 16:01:01', '2020-10-06 13:01:01', '', 0, 'http://college.test/?p=7', 0, 'post', '', 0),
(8, 1, '2020-10-06 16:01:01', '2020-10-06 13:01:01', '<!-- wp:paragraph -->\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n<!-- /wp:paragraph -->', 'Second Test Post', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2020-10-06 16:01:01', '2020-10-06 13:01:01', '', 7, 'http://college.test/2020/10/06/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2020-10-06 16:15:56', '2020-10-06 13:15:56', '<!-- wp:paragraph -->\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>\n<!-- /wp:paragraph -->', 'Test Page 123', '', 'publish', 'closed', 'closed', '', 'test-page-123', '', '', '2020-10-06 16:15:56', '2020-10-06 13:15:56', '', 0, 'http://college.test/?page_id=9', 0, 'page', '', 0),
(10, 1, '2020-10-06 16:15:56', '2020-10-06 13:15:56', '<!-- wp:paragraph -->\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>\n<!-- /wp:paragraph -->', 'Test Page 123', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2020-10-06 16:15:56', '2020-10-06 13:15:56', '', 9, 'http://college.test/2020/10/06/9-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2020-10-06 16:19:20', '2020-10-06 13:19:20', '<!-- wp:paragraph -->\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>\n<!-- /wp:paragraph -->', 'Test Page 123', '', 'inherit', 'closed', 'closed', '', '9-autosave-v1', '', '', '2020-10-06 16:19:20', '2020-10-06 13:19:20', '', 9, 'http://college.test/2020/10/06/9-autosave-v1/', 0, 'revision', '', 0),
(13, 1, '2020-10-07 15:33:05', '2020-10-07 12:33:05', '<!-- wp:paragraph -->\n<p>This is privacy policy page.</p>\n<!-- /wp:paragraph -->', 'Privacy policy', '', 'publish', 'closed', 'closed', '', 'privacy-policy-2', '', '', '2020-10-07 15:33:05', '2020-10-07 12:33:05', '', 0, 'http://college.test/?page_id=13', 0, 'page', '', 0),
(14, 1, '2020-10-07 15:33:05', '2020-10-07 12:33:05', '<!-- wp:paragraph -->\n<p>This is privacy policy page.</p>\n<!-- /wp:paragraph -->', 'Privacy policy', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2020-10-07 15:33:05', '2020-10-07 12:33:05', '', 13, 'http://college.test/2020/10/07/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2020-10-07 16:07:49', '2020-10-07 13:07:49', '<!-- wp:paragraph -->\n<p>This is our history page.</p>\n<!-- /wp:paragraph -->', 'Our History', '', 'publish', 'closed', 'closed', '', 'our-history', '', '', '2020-10-07 16:07:49', '2020-10-07 13:07:49', '', 16, 'http://college.test/?page_id=15', 0, 'page', '', 0),
(16, 1, '2020-10-07 16:07:33', '2020-10-07 13:07:33', '<!-- wp:paragraph -->\n<p>This is about us page</p>\n<!-- /wp:paragraph -->', 'About us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2020-10-07 16:07:33', '2020-10-07 13:07:33', '', 0, 'http://college.test/?page_id=16', 0, 'page', '', 0),
(17, 1, '2020-10-07 16:07:33', '2020-10-07 13:07:33', '<!-- wp:paragraph -->\n<p>This is about us page</p>\n<!-- /wp:paragraph -->', 'About us', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2020-10-07 16:07:33', '2020-10-07 13:07:33', '', 16, 'http://college.test/2020/10/07/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2020-10-07 16:07:49', '2020-10-07 13:07:49', '<!-- wp:paragraph -->\n<p>This is our history page.</p>\n<!-- /wp:paragraph -->', 'Our History', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2020-10-07 16:07:49', '2020-10-07 13:07:49', '', 15, 'http://college.test/2020/10/07/15-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2020-10-07 16:10:03', '2020-10-07 13:10:03', '<!-- wp:paragraph -->\n<p>this is our goals page.</p>\n<!-- /wp:paragraph -->', 'Our Goals', '', 'publish', 'closed', 'closed', '', 'our-goals', '', '', '2020-10-07 17:47:28', '2020-10-07 14:47:28', '', 16, 'http://college.test/?page_id=19', 1, 'page', '', 0),
(20, 1, '2020-10-07 16:10:03', '2020-10-07 13:10:03', '<!-- wp:paragraph -->\n<p>this is our goals page.</p>\n<!-- /wp:paragraph -->', 'Our Goals', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2020-10-07 16:10:03', '2020-10-07 13:10:03', '', 19, 'http://college.test/2020/10/07/19-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2020-10-07 16:39:38', '2020-10-07 13:39:38', '<!-- wp:paragraph -->\n<p>This is cookie policy page.</p>\n<!-- /wp:paragraph -->', 'Cookie policy', '', 'publish', 'closed', 'closed', '', 'cookie-policy', '', '', '2020-10-07 16:39:38', '2020-10-07 13:39:38', '', 13, 'http://college.test/?page_id=21', 0, 'page', '', 0),
(22, 1, '2020-10-07 16:39:38', '2020-10-07 13:39:38', '<!-- wp:paragraph -->\n<p>This is cookie policy page.</p>\n<!-- /wp:paragraph -->', 'Cookie policy', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2020-10-07 16:39:38', '2020-10-07 13:39:38', '', 21, 'http://college.test/2020/10/07/21-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2020-10-07 18:38:11', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:11', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=23', 1, 'nav_menu_item', '', 0),
(24, 1, '2020-10-07 18:38:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:11', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=24', 1, 'nav_menu_item', '', 0),
(25, 1, '2020-10-07 18:38:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:11', '0000-00-00 00:00:00', '', 16, 'http://college.test/?p=25', 1, 'nav_menu_item', '', 0),
(26, 1, '2020-10-07 18:38:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:11', '0000-00-00 00:00:00', '', 16, 'http://college.test/?p=26', 1, 'nav_menu_item', '', 0),
(27, 1, '2020-10-07 18:38:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:11', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=27', 1, 'nav_menu_item', '', 0),
(28, 1, '2020-10-07 18:38:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:11', '0000-00-00 00:00:00', '', 13, 'http://college.test/?p=28', 1, 'nav_menu_item', '', 0),
(29, 1, '2020-10-07 18:38:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:11', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=29', 1, 'nav_menu_item', '', 0),
(30, 1, '2020-10-07 18:38:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:11', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=30', 1, 'nav_menu_item', '', 0),
(31, 1, '2020-10-07 18:38:35', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:35', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=31', 1, 'nav_menu_item', '', 0),
(32, 1, '2020-10-07 18:38:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:35', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=32', 1, 'nav_menu_item', '', 0),
(33, 1, '2020-10-07 18:38:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:35', '0000-00-00 00:00:00', '', 16, 'http://college.test/?p=33', 1, 'nav_menu_item', '', 0),
(34, 1, '2020-10-07 18:38:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:35', '0000-00-00 00:00:00', '', 16, 'http://college.test/?p=34', 1, 'nav_menu_item', '', 0),
(35, 1, '2020-10-07 18:38:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:35', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=35', 1, 'nav_menu_item', '', 0),
(36, 1, '2020-10-07 18:38:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:35', '0000-00-00 00:00:00', '', 13, 'http://college.test/?p=36', 1, 'nav_menu_item', '', 0),
(37, 1, '2020-10-07 18:38:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:35', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=37', 1, 'nav_menu_item', '', 0),
(38, 1, '2020-10-07 18:38:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-10-07 18:38:35', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=38', 1, 'nav_menu_item', '', 0),
(39, 1, '2020-10-07 18:41:08', '2020-10-07 15:41:08', ' ', '', '', 'publish', 'closed', 'closed', '', '39', '', '', '2020-10-07 18:41:08', '2020-10-07 15:41:08', '', 0, 'http://college.test/?p=39', 1, 'nav_menu_item', '', 0),
(40, 1, '2020-10-07 18:41:08', '2020-10-07 15:41:08', ' ', '', '', 'publish', 'closed', 'closed', '', '40', '', '', '2020-10-07 18:41:08', '2020-10-07 15:41:08', '', 0, 'http://college.test/?p=40', 2, 'nav_menu_item', '', 0),
(41, 1, '2020-10-07 18:59:17', '2020-10-07 15:59:17', ' ', '', '', 'publish', 'closed', 'closed', '', '41', '', '', '2020-10-07 18:59:17', '2020-10-07 15:59:17', '', 13, 'http://college.test/?p=41', 1, 'nav_menu_item', '', 0),
(42, 1, '2020-10-07 18:59:17', '2020-10-07 15:59:17', ' ', '', '', 'publish', 'closed', 'closed', '', '42', '', '', '2020-10-07 18:59:17', '2020-10-07 15:59:17', '', 0, 'http://college.test/?p=42', 2, 'nav_menu_item', '', 0),
(43, 1, '2020-10-07 19:00:14', '2020-10-07 16:00:14', '', 'Google', '', 'publish', 'closed', 'closed', '', 'google', '', '', '2020-10-07 19:00:14', '2020-10-07 16:00:14', '', 0, 'http://college.test/?p=43', 2, 'nav_menu_item', '', 0),
(44, 1, '2020-10-07 19:00:14', '2020-10-07 16:00:14', ' ', '', '', 'publish', 'closed', 'closed', '', '44', '', '', '2020-10-07 19:00:14', '2020-10-07 16:00:14', '', 0, 'http://college.test/?p=44', 1, 'nav_menu_item', '', 0),
(45, 1, '2020-10-08 15:15:48', '2020-10-08 12:15:48', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2020-10-08 15:15:48', '2020-10-08 12:15:48', '', 0, 'http://college.test/?page_id=45', 0, 'page', '', 0),
(46, 1, '2020-10-08 15:15:48', '2020-10-08 12:15:48', '', 'Home', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2020-10-08 15:15:48', '2020-10-08 12:15:48', '', 45, 'http://college.test/2020/10/08/45-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2020-10-08 15:16:10', '2020-10-08 12:16:10', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2020-10-08 15:16:10', '2020-10-08 12:16:10', '', 0, 'http://college.test/?page_id=47', 0, 'page', '', 0),
(48, 1, '2020-10-08 15:16:10', '2020-10-08 12:16:10', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2020-10-08 15:16:10', '2020-10-08 12:16:10', '', 47, 'http://college.test/2020/10/08/47-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2020-10-08 15:55:30', '2020-10-08 12:55:30', '<!-- wp:paragraph -->\n<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->', 'We won a award!', '', 'publish', 'open', 'open', '', 'we-won-a-award', '', '', '2020-10-08 15:56:07', '2020-10-08 12:56:07', '', 0, 'http://college.test/?p=49', 0, 'post', '', 0),
(50, 1, '2020-10-08 15:55:30', '2020-10-08 12:55:30', '<!-- wp:paragraph -->\n<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->', 'We won a award!', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2020-10-08 15:55:30', '2020-10-08 12:55:30', '', 49, 'http://college.test/2020/10/08/49-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2020-10-12 17:23:47', '2020-10-12 14:23:47', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Math Meetup', 'Learn science.', 'publish', 'closed', 'closed', '', 'math-meetup', '', '', '2020-10-15 15:44:07', '2020-10-15 12:44:07', '', 0, 'http://college.test/?post_type=event&#038;p=52', 0, 'event', '', 0),
(53, 1, '2020-10-12 17:24:30', '2020-10-12 14:24:30', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.', 'The Science of cats', 'Sweet kitty cat.', 'publish', 'closed', 'closed', '', 'the-science-of-cats', '', '', '2020-10-19 16:01:27', '2020-10-19 13:01:27', '', 0, 'http://college.test/?post_type=event&#038;p=53', 0, 'event', '', 0),
(54, 1, '2020-10-12 17:25:20', '2020-10-12 14:25:20', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.', 'Poetry day', 'Poetry day is amazing.', 'publish', 'closed', 'closed', '', 'poetry-day', '', '', '2020-10-15 15:51:27', '2020-10-15 12:51:27', '', 0, 'http://college.test/?post_type=event&#038;p=54', 0, 'event', '', 0),
(56, 1, '2020-10-14 17:05:27', '2020-10-14 14:05:27', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:5:\"event\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Event Date', 'event-date', 'publish', 'closed', 'closed', '', 'group_5f8705206f420', '', '', '2020-10-14 18:20:33', '2020-10-14 15:20:33', '', 0, 'http://college.test/?post_type=acf-field-group&#038;p=56', 0, 'acf-field-group', '', 0),
(57, 1, '2020-10-14 17:05:27', '2020-10-14 14:05:27', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"d/m/Y\";s:13:\"return_format\";s:5:\"d/m/Y\";s:9:\"first_day\";i:1;}', 'Event Date', 'event_date', 'publish', 'closed', 'closed', '', 'field_5f87053828e55', '', '', '2020-10-14 18:20:33', '2020-10-14 15:20:33', '', 56, 'http://college.test/?post_type=acf-field&#038;p=57', 0, 'acf-field', '', 0),
(58, 1, '2020-10-15 16:25:12', '2020-10-15 13:25:12', 'Functions for Time and Date accept <strong>format string</strong> as a parameter in order to override default Date and Time formatting for certain places in theme or plugin files.', 'Winter Study', '', 'publish', 'closed', 'closed', '', 'winter-study', '', '', '2020-10-15 16:25:12', '2020-10-15 13:25:12', '', 0, 'http://college.test/?post_type=event&#038;p=58', 0, 'event', '', 0),
(59, 1, '2020-10-15 16:46:11', '2020-10-15 13:46:11', '', 'Past Events', '', 'publish', 'closed', 'closed', '', 'past-events', '', '', '2020-10-15 16:46:11', '2020-10-15 13:46:11', '', 0, 'http://college.test/?page_id=59', 0, 'page', '', 0),
(60, 1, '2020-10-15 16:46:11', '2020-10-15 13:46:11', '', 'Past Events', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-10-15 16:46:11', '2020-10-15 13:46:11', '', 59, 'http://college.test/2020/10/15/59-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2020-10-15 16:58:48', '2020-10-15 13:58:48', 'Certain WordPress tag functions are used to display or return date and time information; <a href=\"https://developer.wordpress.org/reference/functions/the_date/\">the_date()</a> and <a href=\"https://developer.wordpress.org/reference/functions/the_time/\">the_time()</a> are examples of this.', 'New year 2019', '', 'publish', 'closed', 'closed', '', 'new-year-2019', '', '', '2020-10-15 16:58:48', '2020-10-15 13:58:48', '', 0, 'http://college.test/?post_type=event&#038;p=62', 0, 'event', '', 0),
(63, 1, '2020-10-19 15:01:11', '2020-10-19 12:01:11', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', 'Math', '', 'publish', 'closed', 'closed', '', 'path', '', '', '2020-10-19 16:01:14', '2020-10-19 13:01:14', '', 0, 'http://college.test/?post_type=program&#038;p=63', 0, 'program', '', 0),
(64, 1, '2020-10-19 15:01:44', '2020-10-19 12:01:44', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.', 'Biology', '', 'publish', 'closed', 'closed', '', 'bioligy', '', '', '2020-10-19 16:10:48', '2020-10-19 13:10:48', '', 0, 'http://college.test/?post_type=program&#038;p=64', 0, 'program', '', 0),
(65, 1, '2020-10-19 15:02:09', '2020-10-19 12:02:09', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.', 'English', '', 'publish', 'closed', 'closed', '', 'english', '', '', '2020-10-19 15:02:09', '2020-10-19 12:02:09', '', 0, 'http://college.test/?post_type=program&#038;p=65', 0, 'program', '', 0),
(67, 1, '2020-10-19 15:21:16', '2020-10-19 12:21:16', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:5:\"event\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Related Program', 'related-program', 'publish', 'closed', 'closed', '', 'group_5f8d843994c69', '', '', '2020-10-19 15:21:16', '2020-10-19 12:21:16', '', 0, 'http://college.test/?post_type=acf-field-group&#038;p=67', 0, 'acf-field-group', '', 0),
(68, 1, '2020-10-19 15:21:16', '2020-10-19 12:21:16', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"program\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'Related programs', 'related_programs', 'publish', 'closed', 'closed', '', 'field_5f8d844500307', '', '', '2020-10-19 15:21:16', '2020-10-19 12:21:16', '', 67, 'http://college.test/?post_type=acf-field&p=68', 0, 'acf-field', '', 0),
(69, 1, '2020-11-09 16:00:38', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-11-09 16:00:38', '0000-00-00 00:00:00', '', 0, 'http://college.test/?p=69', 0, 'post', '', 0),
(70, 1, '2020-11-09 16:06:59', '2020-11-09 13:06:59', '<!-- wp:paragraph -->\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n<!-- /wp:paragraph -->', 'Dummy Post', '', 'publish', 'open', 'open', '', 'dummy-post', '', '', '2020-11-09 16:06:59', '2020-11-09 13:06:59', '', 0, 'http://college.test/?p=70', 0, 'post', '', 0),
(71, 1, '2020-11-09 16:06:59', '2020-11-09 13:06:59', '<!-- wp:paragraph -->\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n<!-- /wp:paragraph -->', 'Dummy Post', '', 'inherit', 'closed', 'closed', '', '70-revision-v1', '', '', '2020-11-09 16:06:59', '2020-11-09 13:06:59', '', 70, 'http://college.test/2020/11/09/70-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2020-11-09 18:37:44', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-11-09 18:37:44', '0000-00-00 00:00:00', '', 0, 'http://college.test/?post_type=event&p=72', 0, 'event', '', 0),
(73, 1, '2020-11-09 19:03:48', '2020-11-09 16:03:48', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:5:\"event\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Event Location', 'event-location', 'publish', 'closed', 'closed', '', 'group_5fa9678cb9a4a', '', '', '2020-11-09 20:32:40', '2020-11-09 17:32:40', '', 0, 'http://college.test/?post_type=acf-field-group&#038;p=73', 1, 'acf-field-group', '', 0),
(74, 1, '2020-11-09 19:03:48', '2020-11-09 16:03:48', 'a:9:{s:4:\"type\";s:10:\"google_map\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:10:\"center_lat\";s:0:\"\";s:10:\"center_lng\";s:0:\"\";s:4:\"zoom\";s:0:\"\";s:6:\"height\";s:0:\"\";}', 'Event Location', 'event_location', 'publish', 'closed', 'closed', '', 'field_5fa967b9de4e5', '', '', '2020-11-09 20:32:40', '2020-11-09 17:32:40', '', 73, 'http://college.test/?post_type=acf-field&#038;p=74', 0, 'acf-field', '', 0),
(75, 1, '2020-11-09 20:27:35', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-11-09 20:27:35', '0000-00-00 00:00:00', '', 0, 'http://college.test/?post_type=event&p=75', 0, 'event', '', 0),
(76, 1, '2020-11-09 20:31:20', '0000-00-00 00:00:00', '', 'Disco night', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-11-09 20:31:20', '2020-11-09 17:31:20', '', 0, 'http://college.test/?post_type=event&#038;p=76', 0, 'event', '', 0),
(77, 1, '2020-11-10 18:11:52', '2020-11-10 15:11:52', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.', 'Party', '', 'publish', 'closed', 'closed', '', 'party', '', '', '2020-11-10 18:11:52', '2020-11-10 15:11:52', '', 0, 'http://college.test/?post_type=event&#038;p=77', 0, 'event', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'My Main Header Menu', 'my-main-header-menu', 0),
(3, 'My Footer Menu 1', 'my-footer-menu-1', 0),
(4, 'My Footer Menu 2', 'my-footer-menu-2', 0),
(5, 'Awards', 'awards', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(5, 1, 0),
(7, 1, 0),
(39, 2, 0),
(40, 2, 0),
(41, 3, 0),
(42, 3, 0),
(43, 4, 0),
(44, 4, 0),
(49, 5, 0),
(70, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 4),
(2, 2, 'nav_menu', '', 0, 2),
(3, 3, 'nav_menu', '', 0, 2),
(4, 4, 'nav_menu', '', 0, 2),
(5, 5, 'category', 'Category description is here!', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'Hugo'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', 'Here is my Bio info.'),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:4:{s:64:\"354b423c4e4964f1d08f70018aa56dd52d03833bfc890cee9efd55eefb03516f\";a:4:{s:10:\"expiration\";i:1606136437;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36\";s:5:\"login\";i:1604926837;}s:64:\"ae941199f75bb58cdb3c7c0b9ea24eeafd0755666ddb43b3b8d72c1f8dc5fa50\";a:4:{s:10:\"expiration\";i:1605110072;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36\";s:5:\"login\";i:1604937272;}s:64:\"650a2d55912fc78f890d988b5e8a53fe4a03d1b45c8b0995038e7b3113b6e625\";a:4:{s:10:\"expiration\";i:1605110080;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36\";s:5:\"login\";i:1604937280;}s:64:\"0f6ed717c343a57f7b516d0c2f31dd7f3f5d0b02eff57bbbaabfdd1b20c91638\";a:4:{s:10:\"expiration\";i:1605110089;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36\";s:5:\"login\";i:1604937289;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '69'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(21, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(22, 1, 'closedpostboxes_event', 'a:0:{}'),
(23, 1, 'metaboxhidden_event', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'hugo', '$P$BBTfPbhHmab.EFZXCzCd9cc9oxo5VE/', 'hugo', 'hugoman@abv.bg', 'http://college.test', '2020-10-06 11:26:41', '', 0, 'Hugo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=432;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
