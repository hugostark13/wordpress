<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'college' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'oE/_2.L6E[p^a$aw{GYOF=L]&Ke?#CXX{3P@-Zqy~N6Ul[/n8c65qghW~LcWs03T' );
define( 'SECURE_AUTH_KEY',  'S~7B Hf1t*(QQ~[Nhp7yPAgA6JC/7km3ULeW7[yp(kJznd(d&K]uO*0n&0AkRsvw' );
define( 'LOGGED_IN_KEY',    'zJX@v|~*Mh>/%2G(%iZ&|fqR/zV.EVC=w(A7BCc6VV-pMCgdOTak {,5Vf[z -n<' );
define( 'NONCE_KEY',        'SEQ*UJ#f>jU4d>Ohv|{BBI5F>pc#{gBFuMRrxZpJ(L)@384N[SvGrPp8+-|?Q{If' );
define( 'AUTH_SALT',        '3m3|U,v<V,kx!@TxXmc9N:rgvSzt&cM:k(=fl6K:sZYyyn}=NuF#{h||YlCp;[vs' );
define( 'SECURE_AUTH_SALT', '<pl{TvH}y%Um#woh[D-,1ZF|#@5xtaI+A^}KnO{5YyA3hY}`x`$:x:G1;b4IgnRA' );
define( 'LOGGED_IN_SALT',   '<g0J:{*]vRL2wE>sw]LQ6s+N|4A3PCmvWT=B8KIxMa }Ewvo9*S?Vtl616X+B2{8' );
define( 'NONCE_SALT',       'e_Cm~;*dGFBxt%40U/?cj_<RhTZe,7vm5%Sd0FqzvR9W&_L+ug.hQ$m2(V6C%6BJ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
