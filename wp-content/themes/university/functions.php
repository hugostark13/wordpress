<?php

function university_files()
{
    wp_enqueue_script('main_university_js', get_theme_file_uri('js/scripts-bundled.js'), null, microtime(), true);
    wp_enqueue_style('google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');   
    wp_enqueue_style('university_main_styles', get_stylesheet_uri(), null, microtime());   
}

add_action('wp_enqueue_scripts', 'university_files');

function university_features()
{
   add_theme_support('title-tag');
//    register_nav_menu('header-menu-location', 'Header Menu Location');
//    register_nav_menu('footer-menu-location-1', 'Footer Menu Location 1');
//    register_nav_menu('footer-menu-location-2', 'Footer Menu Location 2');
}

add_action('after_setup_theme', 'university_features');

function university_adjust_queries($query)
{
  if(!is_admin() AND is_post_type_archive('program') AND $query->is_main_query()){
    $query->set('orderby', 'title');
    $query->set('order', 'ASC');
    $query->set('posts_per_page', -1);
  }
    if(!is_admin() AND is_post_type_archive('event') AND $query->is_main_query()){
        $today = date('Ymd');
        $query->set('meta_key','event_date');
        $query->set('orderby','meta_value_num');
        $query->set('order','ASC');
        $query->set('meta_query', array(
            array(
              'key' => 'event_date',
              'compare' => '>=',
              'value' => $today,
              'type' => 'numeric'
            )
          ));
    }
}

add_action('pre_get_posts', 'university_adjust_queries');

function my_acf_google_map_api( $api ){
  $api['key'] = 'AIzaSyB1m3VvGzLanrADuWcZJGBWqMyf--IQ2TM';
  return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');